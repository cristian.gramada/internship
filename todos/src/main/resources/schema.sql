DROP TABLE IF EXISTS todos;


-- TODOS
CREATE TABLE todos (
  id                INTEGER         NOT NULL PRIMARY KEY,
  description       VARCHAR(50),
  completed         BOOLEAN
);

INSERT INTO todos (id, description, completed) VALUES (1, 'Buy milk', FALSE);
INSERT INTO todos (id, description, completed) VALUES (2, 'Take dog out', TRUE);
INSERT INTO todos (id, description, completed) VALUES (3, 'Clean dishes', TRUE);
