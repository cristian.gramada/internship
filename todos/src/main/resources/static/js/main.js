var xhttp = new XMLHttpRequest();

xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {

        var list = JSON.parse(xhttp.responseText)['data'];
        var body = document.getElementsByTagName('body')[0];

        var items = '';
        list.forEach(todo => {
            items += '<li>' + todo.description + '</li>';
        });

        body.innerHTML += items;
    }
};

xhttp.open("GET", "/list", true);
xhttp.send();