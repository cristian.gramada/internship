package org.home.todos;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TodosApplications {

	public static void main(String[] args) {
		SpringApplication.run(TodosApplications.class, args);
	}
}
