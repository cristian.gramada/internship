package org.home.todos;

public class Todo {

    private final long id;
    private final String description;
    private final Boolean completed;

    public Todo(long id, String description, Boolean completed) {
        this.id = id;
        this.description = description;
        this.completed = completed;
    }

    public long getId() {
        return id;
    }

    public String getDescription() {
        return description;
    }

    public Boolean getCompleted() {
        return completed;
    }
}
