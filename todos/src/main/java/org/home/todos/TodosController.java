package org.home.todos;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
public class TodosController {

    JdbcTemplate jdbcTemplate;

    public TodosController(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @GetMapping("/list")
    public Map<String, List<Todo>> list() {
        String query = "SELECT id, description, completed FROM todos";
        RowMapper<Todo> todoRowMapper = (rs, rowNum) -> new Todo(
                rs.getLong("id"),
                rs.getString("description"),
                rs.getBoolean("completed")
        );

        HashMap<String, List<Todo>> response = new HashMap<>();
        response.put("data", jdbcTemplate.query(query, todoRowMapper));

        return response;
    }
}
